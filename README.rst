=====
DJANGO GRAILS INTEGRATION
=====

a collection of utils to use user-management and user-credential tables of grails/spring based application (so applications
that are using spring-security). the goal for having this app is to ease migration from spring to django. 

currently there are no plans to automate model transformation since that is better be done by using: python3 manage.py inspectdb

NOTE: this package assumes grails/spring legacy database is mysql (should be ease to adopt for other products).


Quick start
-----------
1. install the package

2. add app to settings like:

INSTALLED_APPS = (
    …,
    'grails_support',
)


3. use views/models and mgmnt scripts of the package


NOTE
-----------
1. used for for django >= 2.1
