#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from collections import OrderedDict
from django.contrib.auth.hashers import BasePasswordHasher, mask_hash
from django.utils.crypto import constant_time_compare
from django.utils.translation import ugettext_lazy as _
import hashlib


class UnsaltedSHA256PasswordHasher(BasePasswordHasher):
    """
    use this hasher when you want to move your user from django/spring to django.
    passwords are updated to an better hashing-algo as soon as the user authenticates.

    be sure to understand django mechanics to update password hashes.
    you may have a look into tests of this project.
    """

    algorithm = "unsalted_sha256"

    def salt(self):
        return ''

    def encode(self, password, salt):
        assert salt == ''
        hash = hashlib.sha256(password.encode()).hexdigest()
        return 'unsalted_sha256$$%s' % hash

    def verify(self, password, encoded):
        encoded_2 = self.encode(password, '')
        return constant_time_compare(encoded, encoded_2)

    def safe_summary(self, encoded):
        assert encoded.startswith('unsalted_sha256$$')
        hash = encoded[15:]
        return OrderedDict([
            (_('algorithm'), self.algorithm),
            (_('hash'), mask_hash(hash)),
        ])

    def must_update(self, encoded):
        return True

    def harden_runtime(self, password, encoded):
        pass


class GrailsSHA256PasswordHasher(BasePasswordHasher):
    """
    use this hasher if you need to have a working grails instance working with user tables.
    therefor this passwords cannot be updated nor can there be any prefixed to the hashes.

    be sure to understand django mechanics to update password hashes.
    you may have a look into tests of this project.
    """

    algorithm = "grails_unsalted_sha256"

    def salt(self):
        return ''

    def encode(self, password, salt):
        assert salt == ''
        hash = hashlib.sha256(password.encode()).hexdigest()
        return '%s' % hash

    def verify(self, password, encoded):
        encoded_2 = self.encode(password, '')
        return constant_time_compare(encoded, encoded_2)

    def safe_summary(self, encoded):
        hash = encoded
        return OrderedDict([
            (_('algorithm'), self.algorithm),
            (_('hash'), mask_hash(hash)),
        ])

    def must_update(self, encoded):
        return False

    def harden_runtime(self, password, encoded):
        pass
