#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db import connections
from grails_support.exceptions import GrailsIntegrationException
from grails_support.utils import mysql_1bit_bool
from hashid import HashID
import logging


logger = logging.getLogger(__name__)


# singleton hash identifier
hash_identifier = HashID()

DEFAULT_GRAILS_DATABASE_NAME = 'grails_db'

# key == field of django user object
DEFAULT_USER_FIELD_MAPPINGS = {
    'username': ('username', ),
    'first_name': ('name', ),
    'last_name': ('surename', ),
    'email': ('username', ),
    'is_active': ('enabled', mysql_1bit_bool, ),
    'date_joined': ('date_created', ),
}


def get_grails_db_connection():
    """
    :return: opened connection to grails db
    """
    logger.debug("get_grails_db_connection")

    db_name = DEFAULT_GRAILS_DATABASE_NAME
    if hasattr(settings, 'GRAILS_DATABASE_NAME'):
        db_name = settings.GRAILS_DATABASE_NAME

    if db_name not in connections:
        raise ImproperlyConfigured('expected database configuration for: {}'.format(db_name))

    return connections[db_name]


def get_grails_users():
    """
    query to user-table
    :return: results as dict
    """
    logger.debug("get_grails_users")

    con = None
    try:
        con = get_grails_db_connection()
        with con.cursor() as cur:
            cur.execute(f"select * from {settings.GRAILS_USER_TABLE}")
            desc = cur.description
            return [
                dict(zip([col[0] for col in desc], row))
                for row in cur.fetchall()
            ]
    except:
        logger.error("failed to get grails users from: %s", con, exc_info=True)

    finally:
        if con is not None:
            con.close()


def get_user_field_mappings():
    """
    gets field mappings either from settings 'GRAILS_USER_TABLE_MAPPINGS' if that cannot be found
    DEFAULT_USER_FIELD_MAPPINGS return returned
    """
    logger.debug("get_user_field_mappings")

    if hasattr(settings, 'GRAILS_USER_TABLE_MAPPINGS'):
        return settings.GRAILS_USER_TABLE_MAPPINGS

    return DEFAULT_USER_FIELD_MAPPINGS


def upt_django_user_fields(user, source, field_mappings=None):
    """
    to update all fields except password of a django user object.
    NOTE: this method does not persist changes.

    :param user: django user to be updated
    :param source: data dict that holds data to inserted
    :param mapping: dict containing mapping - key of this dict is field-name of django user
    :return: updated obj
    """

    mappings = field_mappings
    if mappings is None:
        mappings = get_user_field_mappings()

    logger.debug("upt_django_user_fields user: %s, source: %s, mappings: %s", user, source, mappings)

    # check for password field
    if 'password' in [it.lower() for it in mappings.keys()]:
        raise NotImplemented("this method does not support modification of password fields - "
                             "use update_django_user_password for that!")

    for mapping in mappings:
        mapping_field_name = mappings[mapping][0]
        value = source[mapping_field_name]

        # let's if there is a converter
        if len(mappings[mapping]) > 1:
            converter = mappings[mapping][1]
            value = converter(value)

        setattr(user, mapping, value)

    return user


def update_django_user_password(user, password_hash):
    """
    use this to update django user passwords this hashes coming from grails/spring security.
    therefore the given password_hash is checked against hash_identifier (hashID) and then inserted into
    django users password column.

    NOTE: this method does not persist changes.

    BASICS FOR DJANGO PASSWORD HANDLING:
    Django provides a flexible password storage system and uses PBKDF2 by default.

    The password attribute of a User object is a string in this format:
    <algorithm>$<iterations>$<salt>$<hash>

    example:
    pbkdf2_sha256$150000$WKD1jNnZoMlg$xuFauzhooX3UEot6J1RwbRMQ7g9y+Mbx1JkcEccdHF0=

    get more information here: https://docs.djangoproject.com/en/2.2/topics/auth/passwords/

    :param user: django user to be updated
    :param password_hash: hash from password
    :return: updated user object
    """
    logger.debug("update_django_user_password user: %s, password_hash: %s", user, password_hash)

    password_hash_algo = 'SHA-256'

    hash_ids = list(hash_identifier.identifyHash(password_hash))
    if password_hash_algo not in [it.name for it in hash_ids]:
        raise GrailsIntegrationException(f'algo: {password_hash_algo} seems not good for hash: {password_hash}')

    # set value to password field of django user
    # NOTE: UnsaltedSHA256PasswordHasher is used here
    user.password = "unsalted_sha256$$%s" % password_hash
    return user
