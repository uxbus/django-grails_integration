#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from grails_support.user_services import get_grails_users, upt_django_user_fields, update_django_user_password, \
    get_user_field_mappings
import logging


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'import grails users as django users / keeping passwords and other fields'

    def add_arguments(self, parser):
        parser.add_argument('update_existing', type=bool, nargs='?', default=False,
                            help='set this to true is you want to modify already existing entries')

    def handle(self, *args, **options):
        qs = get_grails_users()
        if len(qs) == 0:
            print("DONE: no users in grails user-table")
            return

        user_model = get_user_model()
        user_field_mapping = get_user_field_mappings()
        for it in qs:
            username = it[user_field_mapping['username'][0]]
            logger.debug("username: %s", username)

            try:
                if user_model.objects.filter(username=username).exists():
                    if not options.get('update_existing'):
                        logger.info("user with username: %s already exists", username)
                        continue

                    new_django_user = user_model.objects.get(username=username)

                else:
                    new_django_user = user_model()

                new_django_user = update_django_user_password(
                    upt_django_user_fields(new_django_user, it),
                    it['password']
                )

                new_django_user.save()

                logger.info("created django user: %s", new_django_user)
            except:
                logger.error("failed to import grails user: %s", it, exc_info=True)
