#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.core.management import BaseCommand
from grails_support.user_services import get_grails_users
import logging


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'simple util to find a grails user and print username, email and password columns of grails table'

    def add_arguments(self, parser):
        parser.add_argument('find', type=str, nargs='?', default='')

    def handle(self, *args, **options):
        to_find = options.get('find')
        logger.debug("to_find: %s", to_find)

        for it in get_grails_users():
            if to_find in str(it):
                print(it['username'], '\t', it['password'])
