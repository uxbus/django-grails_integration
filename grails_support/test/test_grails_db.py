#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from grails_support.user_services import get_grails_db_connection, get_grails_users
import pytest


@pytest.mark.django_db
def test_get_grails_db_connection():

    ret = get_grails_users()
    assert len(ret) == 10
