#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.conf import settings
from django.db import connections
import logging
import pytest


logger = logging.getLogger(__name__)


sql_create = """create table person (
    id                             bigint auto_increment primary key,
    account_expired                bit          not null,
    account_locked                 bit          not null,
    activation_date                datetime     null,
    activation_hash                varchar(40)  null,
    address_country                varchar(255) null,
    address_place                  varchar(200) not null,
    address_street                 varchar(100) not null,
    address_street_number          varchar(20)  not null,
    address_zip_code               varchar(20)  not null,
    copies                         varchar(255) null,
    created_by_id                  bigint       null,
    enabled                        bit          not null,
    listview                       int          not null,
    lost_pass_date                 datetime     null,
    lost_pass_hash                 varchar(40)  null,
    media                          varchar(255) null,
    name                           varchar(200) not null,
    optin_date                     datetime     null,
    optin_hash                     varchar(40)  null,
    password                       varchar(255) not null,
    password_expired               bit          not null,
    profile_id                     bigint       null,
    publication                    varchar(255) null,
    purpose                        varchar(255) null,
    ressort                        varchar(255) null,
    salutation                     varchar(6)   not null,
    subscription_date              datetime     null,
    surename                       varchar(200) not null,
    telefon_area                   varchar(6)   null,
    telefon_number                 varchar(20)  null,
    telefon_prefix                 varchar(4)   null,
    username                       varchar(255) not null,
    wants_newsletter               bit          not null,
    wants_updates                  bit          not null,
    accept_terms_of_use            bit          not null,
    accept_privacy_policy          bit          not null,
    registration_question_id       bigint       null,
    date_created                   datetime     null,
    last_updated                   datetime     null,
    additional_information         varchar(255) null,
    agency                         varchar(255) null,
    can_submit_product_suggestions char         null,
    company                        varchar(255) null,
    department                     varchar(255) null,
    employment_type                varchar(255) null,
    fax_area                       varchar(6)   null,
    fax_number                     varchar(20)  null,
    fax_prefix                     varchar(4)   null,
    telefon2_area                  varchar(6)   null,
    telefon2_number                varchar(20)  null,
    telefon2_prefix                varchar(4)   null,
    cinema                         varchar(255) null,
    cinema_chain                   varchar(255) null,
    cinema_function                varchar(255) null,
    constraint username unique (username)
)charset = utf8;"""


@pytest.fixture(autouse=True)
def insert_some_users(django_db_setup):
    logger.warning("insert_some_users INTO grails_db")
    with connections['grails_db'].cursor() as cur:
        cur.execute(sql_create)

        for x in range(10):
            sql = """insert into {} (password, salutation, username, name, surename, enabled, account_expired, 
            account_locked, password_expired, activation_date, address_street, address_street_number, address_zip_code, 
            address_place, listview, wants_newsletter, wants_updates, accept_terms_of_use, accept_privacy_policy)
            values (
            '9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9', 'female', 'user{}@email.com', 
            'first_name', 'last_name', true, false, false, false, now(), 'street', 'nr', '10245', 'berlin', 0, 
            true, true, true, true)""".format(settings.GRAILS_USER_TABLE, x)

            cur.execute(sql)
