#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.contrib.auth import authenticate, hashers
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from grails_support.hashers import UnsaltedSHA256PasswordHasher, GrailsSHA256PasswordHasher
import pytest


@pytest.mark.django_db
def test_md5_passwd_upgrade(admin_user):
    admin_user.password = make_password(password='testing', salt=None, hasher='unsalted_md5')
    admin_user.save()

    print("md5_pass", admin_user.password)

    authenticate(username='tester', password='testing')
    print("after", User.objects.get(id=admin_user.id).password)


@pytest.mark.django_db
def test_unsalted_sha256_passwd_upgrade(admin_user):

    # ensure hash is configured
    configured_hashers = hashers.get_hashers()
    assert any(isinstance(x, UnsaltedSHA256PasswordHasher) for x in configured_hashers)

    # after migration from grails we maid have a weaker password hash … example sha-256 / iterations 1 / no salt
    admin_user.password = make_password(password='hua', salt=None, hasher='unsalted_sha256')
    admin_user.save()

    # SHA256 ("hua") = 9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9
    assert admin_user.password == "unsalted_sha256$$9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9"

    # after a little while admin comes and authenticates
    authenticate(username=admin_user.username, password='hua')

    # refresh and verify password has been updated
    assert User.objects.get(id=admin_user.id).password != "unsalted_sha256$$9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9"

    print("upgraded password", User.objects.get(id=admin_user.id).password)


@pytest.mark.django_db
def test_grails_sha256_passwd_upgrade(admin_user):

    # ensure hash is configured
    configured_hashers = hashers.get_hashers()
    assert any(isinstance(x, GrailsSHA256PasswordHasher) for x in configured_hashers)

    # after migration from grails we maid have a weaker password hash … example sha-256 / iterations 1 / no salt
    admin_user.password = make_password(password='hua', salt=None, hasher='grails_unsalted_sha256')
    admin_user.save()

    assert admin_user.password == '9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9'

    # after a little while admin comes and authenticates
    authenticate(username=admin_user.username, password='hua')

    # refresh and verify password has NOT been updated
    assert User.objects.get(id=admin_user.id).password == "9756c838f33fd5f72e8a709aaec323bc8f8a6a947fb905947fc1e524154668e9"
