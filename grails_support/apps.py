from django.apps import AppConfig


class GrailsSupportConfig(AppConfig):
    name = 'grails_support'
