#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.utils import timezone
import logging


logger = logging.getLogger(__name__)


def mysql_1bit_bool(value):
    """
    simple convert for mysql bit boolean type
    :return: bool
    """
    logger.debug("mysql_1bit_bool value: %s", value)
    return value == b'\x01'


def yes_no_char_bool(value):
    """
    simple convert for mysql char (Y|N) boolean type
    :return: bool
    """
    logger.debug("yes_no_char_bool value: %s", value)

    if value is None:
        return False

    return value.lower() == 'y'


def value_or_current_datetime(value):
    """
    simple converter that returns either the value or current datetime
    :return: datetime
    """
    logger.debug("value_or_current_datetime value: %s", value)

    if value:
        return value
    return timezone.now()
