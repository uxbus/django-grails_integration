#       _____  _____  __ __  _____  _____  _____  _____
#      |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#      |   __||     ||_   _||     ||   __||   __||     |
#      |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#      ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#      Copyright (c) 2019. All rights reserved.
#
from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include, re_path
from django.conf.urls.static import static
import os


urlpatterns = [
    path('', admin.site.urls),

    # NOTE: this may be improved by adding it apache or nginx hosting
    re_path(r'^robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow: /\n", content_type="text/plain")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG or 'DEBUGBAR' in os.environ.keys():
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)),] + urlpatterns
